# build for multi-stage image
FROM nrvale0/hashitools-oss:latest as build

ENV PATH /bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
	apt-get install -y wget curl httpie make

# kubectl
RUN cd /usr/local/bin && \
	curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl && \
	chmod +x kubectl

# inspec
RUN curl https://omnitruck.chef.io/install.sh | bash -s -- -P inspec

# goss
RUN curl -fsSL https://goss.rocks/install | sh

# k8s helm
RUN set -eux && \
	wget -cq -O - https://get.helm.sh/helm-v3.0.2-linux-amd64.tar.gz | \
	tar zxvf - -C /usr/local/bin/ --strip-components=1 linux-amd64/helm && \
	chmod +x /usr/local/bin/helm

COPY ./Dockerfile /Dockerfile

MAINTAINER Nathan Valentine
LABEL email="nrvale0@protonamil.com" \
 	repo="https://gitlab.com/nrvale0/docker-hashitools-oss-plus-ecosystem"
